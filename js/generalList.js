function contador() {

    var fecha_entrada = $('#diaEntradaPaciente').val();
    var fecha_salida = $('#diaSalidaPaciente').val();
    if (fecha_entrada && fecha_salida) {
        var aFecha1 = moment(fecha_entrada);
        var aFecha2 = moment(fecha_salida);
        var diferencia = aFecha2.diff(aFecha1, 'days');
        $('#diasAtencion').val(diferencia);
    } else {
        $('#diasAtencion').val("0");
    }
}

function selectAplica(sel) {
    var selected = sel.options[sel.selectedIndex].text;
    var idcriterio = sel.attributes[4].value;
    if ((selected == "SI") || (selected == "NO")) {
        var aplicaAdherencia = $('input#aplicaFormula[idcriterio="' + idcriterio + '"]').prop('checked', true);
    } else {
        var aplicaAdherencia = $('input#aplicaFormula[idcriterio="' + idcriterio + '"]').prop('checked', false);
    }
}

function selectSubAplica(sel) {
    var selected = sel.options[sel.selectedIndex].text;
    var idcriterio = sel.attributes[4].value;
    if ((selected == "SI") || (selected == "NO")) {
        var aplicaAdherencia = $('input#subAplicaFormula[idSubCriterio="' + idcriterio + '"]').prop('checked', true);
    } else {
        var aplicaAdherencia = $('input#subAplicaFormula[idSubCriterio="' + idcriterio + '"]').prop('checked', false);
    }
}

window.addEventListener('load', fecha, false);

function fecha() {

    var select = $('#tipoLista');

    $.ajax({
        type: "POST",
        url: "BD/getLista.php",
        success: function(respuesta) {
            var listas = jQuery.parseJSON(respuesta);
            $.each(listas, function(key, value) {
                select.append('<option value=' + value.idLista + ' consecutivo=' + value.consecutivoLista + ' subLista=' + value.subLista + '>' + value.nombreLista + '</option>');
            });
        }
    });

    var f = new Date();
    var D = "" + (f.getDate());
    var DD = "0" + D;
    DD = DD.substring(DD.length - 2, DD.length);
    var M = "" + (f.getMonth() + 1);
    var MM = "0" + M;
    MM = MM.substring(MM.length - 2, MM.length);
    var fecha = MM + DD;
    document.getElementById('fechaDiagnostico').value = fecha;

    $('.timePicker').datetimepicker({
        format: 'HH:mm'
    });
}

function tipoLista(sel) {
    var selected = sel.options[sel.selectedIndex].text;

    $('#titulo1').text("Lista de Chequeo " + selected);

    var subLista = $("#tipoLista option:selected").attr("subLista");
    var consecutivo = $("#tipoLista option:selected").attr("consecutivo");
    $('#concecutivo').val(consecutivo);
    datosPaciente = document.getElementById("datosPaciente");
    datosPaciente.style.display = "";
    if (subLista == 1) {

        var idLista = sel.options[sel.selectedIndex].value;
        var dataString = 'idLista=' + idLista;
        var selectSubLista = $('#selectsubLista');

        $.ajax({
            type: "POST",
            url: "BD/getSubLista.php",
            data: dataString,
            dataType: "json",
            success: function(respuesta) {

                var cantidadOpnciones = $("#selectsubLista").find("option").length;
                var opciones = $("#selectsubLista").find("option").length;
                if (cantidadOpnciones == 1) {
                    $.each(respuesta, function(key, value) {
                        selectSubLista.append("<option value=" + value.idsubLista + " subLista='1'>" + value.NombreSubLista + "</option>");
                    });
                } else {
                    for (var i = 0; i < cantidadOpnciones; i++) {
                        $("#selectsubLista").find("option[value='" + i + "']").remove();
                    }
                    selectSubLista.append("<option value='0' subLista='1'></option>");
                    $.each(respuesta, function(key, value) {
                        selectSubLista.append("<option value=" + value.idsubLista + " subLista='1'>" + value.NombreSubLista + "</option>");
                    });
                }
            }
        });

        $.ajax({
            type: "POST",
            url: "BD/selectLista.php",
            data: dataString,
            dataType: "json",
            success: function(respuesta) {
                if ($('div#dominio').length != 0) {
                    $("div#dominio").remove();
                    $("div#criterio").remove();
                    $("select#respuesta").remove();
                    $("input#observaciones").remove();
                    $("div#checkbox").remove();
                }
                var criterioslista;

                var numDominio = 1;
                var height = 0;
                $.each(respuesta, function(i, item1) {
                    criterioslista = item1.criterios;

                    var nameClaseDominio = "dominio_" + numDominio;

                    $('#dominioPadre').append("<div class='" + nameClaseDominio + "' id='dominio' style='margin: 5px;'>" + item1.nombredominio + "</div>");
                    var counter = 0;
                    $.each(criterioslista, function(a, item) {
                        var nameCriterio = item.nombrecriterio.length;

                        if (nameCriterio <= 89) {
                            height = 34;
                            if (counter === 1) {
                                $("." + nameClaseDominio).css("height", height);
                            }
                        } else if (nameCriterio >= 90 && nameCriterio <= 100) {
                            height = 45;
                            if (counter === 1) {
                                $("." + nameClaseDominio).css("height", height);
                            }
                        } else if (nameCriterio >= 101 && nameCriterio <= 300) {
                            height = 65;
                            if (counter === 1) {
                                $("." + nameClaseDominio).css("height", height);
                            }
                        } else if (nameCriterio >= 301 && nameCriterio <= 500) {
                            height = 140;
                            if (counter === 1) {
                                $("." + nameClaseDominio).css("height", height);
                            }
                        }

                        $('#criterioPadre').append("<div class='criterio' id='criterio' style='height: " + height + "px;margin: 5px;'>" + item.nombrecriterio + "</div>");
                        counter = counter + 1;
                        var nameClaseDominioFantasma = "dominioFantasma_" + counter;
                        var dominioPunto = "<div class='" + nameClaseDominioFantasma + "' id='dominio' style='height: " + height + "px;margin: 5px;'>.</div>"
                        var checkbox = "<div class='checkbox' id='checkbox' style='height: " + height + "px;margin: 5px;'><input type='checkbox' value='' id='aplicaFormula' idCriterio= '" + item.idCriterio + "' style='margin-left: inherit;' disabled='true'></div>";
                        var select = "<select name='select_tp_dcmt' class='form-control' autofocus='' id='respuesta' idCriterio= '" + item.idCriterio + "' style='margin: 5px;font-size: 10px;' onChange='selectAplica(this)'>\
                      <option value='0'></option>\
                      <option value='1'>SI</option>\
                      <option value='2'>NO</option>\
                      <option value='3'>N/A</option>";
                        if (counter > 1) {
                            $('#dominioPadre').append(dominioPunto);
                        }
                        $('#aplicaCriterio').append(checkbox);
                        $('#padrerespuesta').append(select);
                        var inputobservaciones = "<input type='text' name='' value='' class='form-control' id='observaciones' idCriterio= '" + item.idCriterio + "' style='margin: 5px;'>";
                        $('#padreobservaciones').append(inputobservaciones);
                        height = 0;
                    });
                });
            }
        });
        $("#divSubLista").css("display", "");
    } else {
        $("div#dominio").remove();
        $("div#criterio").remove();
        $("select#respuesta").remove();
        $("input#observaciones").remove();
        $("div#checkbox").remove();

        var subLista = $("#selectsubLista option:selected").attr("subLista");

        if (subLista == 1) {
            $("div#sudDominio").remove();
            $("div#subcriterio").remove();
            $("select#subRespuesta").remove();
            $("input#subobservaciones").remove();
            $("div#subcheckbox").remove();
        }

        $("#subListaLoaded").css("display", "none");
        $("#divSubLista").css("display", "none");

        var value = sel.options[sel.selectedIndex].value;
        var dataString = 'idLista=' + value;

        $.ajax({
            type: "POST",
            url: "BD/selectLista.php",
            data: dataString,
            dataType: "json",
            success: function(respuesta) {
                var criterioslista;
                var numDominio = 0;
                $.each(respuesta, function(i, item1) {
                    var height = 0;
                    criterioslista = item1.criterios;
                    numDominio = numDominio + 1;
                    var nameClaseDominio = "dominio_" + numDominio;
                    var counter = 0;
                    $('#dominioPadre').append("<div class='" + nameClaseDominio + "' id='dominio' style='margin: 5px;'>" + item1.nombredominio + "</div>");
                    $.each(criterioslista, function(a, item) {
                        counter = counter + 1;
                        var nameCriterio = item.nombrecriterio.length;
                        if (nameCriterio <= 89) {
                            height = 34;
                            if (counter === 1) {
                                $("." + nameClaseDominio).css("height", height);
                            }
                        } else if (nameCriterio >= 90 && nameCriterio <= 100) {
                            height = 45;
                            if (counter === 1) {
                                $("." + nameClaseDominio).css("height", height);
                            }
                        } else if (nameCriterio >= 101 && nameCriterio <= 300) {
                            height = 65;
                            if (counter === 1) {
                                $("." + nameClaseDominio).css("height", height);
                            }
                        } else if (nameCriterio >= 301 && nameCriterio <= 500) {
                            height = 140;
                            if (counter === 1) {
                                $("." + nameClaseDominio).css("height", height);
                            }
                        }

                        $('#criterioPadre').append("<div class='criterio' id='criterio' style='height: " + height + "px;margin: 5px;'>" + item.nombrecriterio + "</div>");
                        var nameClaseDominioFantasma = "dominioFantasma_" + counter;
                        var dominioPunto = "<div class='" + nameClaseDominioFantasma + "' id='dominio' style='height: " + height + "px;margin: 5px;'>.</div>"
                        var checkbox = "<div class='checkbox' id='checkbox' style='height: " + height + "px;margin: 5px;'><input type='checkbox' value='' id='aplicaFormula' idCriterio= '" + item.idCriterio + "' style='margin-left: inherit;' disabled='true'></div>";
                        var select = "<select name='select_tp_dcmt' class='form-control' autofocus='' id='respuesta' idCriterio= '" + item.idCriterio + "' style='margin: 5px;height: " + height + "px;font-size: 10px;' onChange='selectAplica(this)'>\
                  <option value='0'></option>\
                  <option value='1'>SI</option>\
                  <option value='2'>NO</option>\
                  <option value='3'>N/A</option>";
                        if (counter > 1) {
                            $('#dominioPadre').append(dominioPunto);
                        }
                        $('#aplicaCriterio').append(checkbox);
                        $('#padrerespuesta').append(select);
                        var inputobservaciones = "<input type='text' name='' value='' class='form-control' id='observaciones' idCriterio= '" + item.idCriterio + "' style='margin: 5px;height: " + height + "px;'>";
                        $('#padreobservaciones').append(inputobservaciones);
                        height = 0;
                    });
                });
            }
        });
    }

    if (selected == "Pacientes") {
        horaIngreso = document.getElementById("horaIngreso");
        horaIngreso.style.display = "none";

        horaEgreso = document.getElementById("horaEgreso");
        horaEgreso.style.display = "none";
    } else {
        horaIngreso = document.getElementById("horaIngreso");
        horaIngreso.style.display = "";

        horaEgreso = document.getElementById("horaEgreso");
        horaEgreso.style.display = "";
    }
}

function loadSubLista(sel) {
    var value = sel.options[sel.selectedIndex].value;
    var dataString = 'idSubLista=' + value;

    $.ajax({
        type: "POST",
        url: "BD/selectSubLista.php",
        data: dataString,
        dataType: "json",
        success: function(respuestasublista) {
            subListaLoaded = document.getElementById("subListaLoaded");
            subListaLoaded.style.display = "";
            var subcriterioslista;
            var numSubDominio = 0;
            $.each(respuestasublista, function(i, item1) {
                var counter = 0;
                var height = 0;
                numSubDominio = numSubDominio + 1;
                var nameClaseSubDominio = "subdominio_" + numSubDominio;
                subcriterioslista = item1.subcriterios;
                var subDominio = "<div class='" + nameClaseSubDominio + "' id='sudDominio' style='height: 34px;margin: 5px;'>" + item1.nameSubdominio + "</div>";
                $('#SubDominioPadre').append(subDominio);

                $.each(subcriterioslista, function(a, item) {
                    counter = counter + 1;
                    var nameCriterio = item.nombresubcriterio.length;
                    if (nameCriterio <= 89) {
                        height = 34;
                        if (counter === 1) {
                            $("." + nameClaseSubDominio).css("height", height);
                        }
                    } else if (nameCriterio >= 90 && nameCriterio <= 100) {
                        height = 45;
                        if (counter === 1) {
                            $("." + nameClaseSubDominio).css("height", height);
                        }
                    } else if (nameCriterio >= 101 && nameCriterio <= 300) {
                        height = 65;
                        if (counter === 1) {
                            $("." + nameClaseSubDominio).css("height", height);
                        }
                    } else if (nameCriterio >= 301 && nameCriterio <= 500) {
                        height = 140;
                        if (counter === 1) {
                            $("." + nameClaseSubDominio).css("height", height);
                        }
                    }

                    var subCriterio = "<div class='subcriterio' id='subcriterio' style='height: " + height + "px;margin: 5px;'>" + item.nombresubcriterio + "</div>";
                    $('#SubCriterioPadre').append(subCriterio);
                    var sudDominioFantasma = "<div class='" + nameClaseSubDominio + "' id='sudDominio' style='height: 34px; margin: 5px;'>.</div>";
                    var nameClaseDominioFantasma = "dominioFantasma_" + counter;
                    var checkbox = "<div class='subcheckbox' id='subcheckbox' style='height: " + height + "px;margin: 5px;'><input type='checkbox' value='' id='subAplicaFormula' idSubCriterio= '" + item.idsubcriterio + "' style='margin-left: inherit;' disabled='true'></div>";
                    var select = "<select name='select_tp_dcmt' class='form-control' autofocus='' id='subRespuesta' idSubCriterio= '" + item.idsubcriterio + "' style='margin: 5px; height:" + height + "px;font-size: 10px;' onChange='selectSubAplica(this)'>\
                  <option value='0'></option>\
                  <option value='1'>SI</option>\
                  <option value='2'>NO</option>\
                  <option value='3'>N/A</option>";
                    if (counter > 1) {
                        $('#SubDominioPadre').append(sudDominioFantasma);
                    }
                    $('#SubAplicaCriterio').append(checkbox);
                    $('#subRespuesta').append(select);
                    var inputobservaciones = "<input type='text' name='' value='' class='form-control' id='subobservaciones' idsubCriterio= '" + item.idsubcriterio + "' style='margin: 5px; height:" + height + "px;'>";
                    $('#subobservaciones').append(inputobservaciones);
                });
            });
        }
    });
}

$(document).ready(function() {
    $("#closeModal").click(function() {
        location.reload();
    });
});

$(document).ready(function() {
    $("#closemodalX").click(function() {
        location.reload();
    });
});


$(document).ready(function() {
    $("#guardarLista").click(function() {
        function validarRespuestasLista(respuestasSeleccionadas) {
            var guardar;
            $.each(respuestasSeleccionadas, function(index1, value) {
                if (value.respuestaCriterio == 0) {
                    toastr.error("Alguno de las preguntas esta sin respuesta, por favor revisar");
                    guardar = false;
                    return false;
                }
            });
            if (guardar == false) {
                return guardar;
            }
            return true;
        }

        function validarSubRespuestasLista(subRespuestasSeleccionadas) {
            var guardar;
            $.each(subRespuestasSeleccionadas, function(index1, value) {
                if (value.respuestaCriterio == 0) {
                    toastr.error("Alguno de las preguntas esta sin respuesta, por favor revisar");
                    guardar = false;
                    return false;
                }
            });
            if (guardar == false) {
                return guardar;
            }
            return true;
        }

        function guardarRespuestasSubLista(ARRAYsubLista_respuestas, idPacienteGenerado) {
            jsonListaRespuestas = JSON.stringify(ARRAYsubLista_respuestas);
            $.ajax({
                async: false,
                cache: false,
                type: "POST",
                url: "BD/guardarRespuestasSubLista.php",
                data: {
                    'data': jsonListaRespuestas,
                    'idPaciente': idPacienteGenerado
                },
                dataType: "json",
                success: function(respuestaInsertSubRespuestas) {
                    if (respuestaInsertSubRespuestas.status) {
                        toastr.success("Sub lista guardada correctamente");
                    }
                }
            });
        }

        function calcularAdherencia(lista, paciente, subLista, subListaID) {
            var infoModal = $('#modalResultado');
            $.ajax({
                type: "POST",
                url: "BD/calcularAdherencia.php",
                data: {
                    'lista': lista,
                    'idPaciente': paciente,
                    'subLista': subLista,
                    'subListaID': subListaID
                },
                dataType: "json",
                success: function(respuestaCalculoAdherencia) {
                    var countContainer = 0;
                    var totalContainers = [];
                    var totalclases = [];
                    var clase;
                    var cumplen;
                    var nocumplen
                    $.each(respuestaCalculoAdherencia, function(index, item) {
                        countContainer = countContainer + 1;
                        var nameContainer = 'container_' + countContainer;
                        clase = 'padre_' + countContainer;
                        totalclases.push(clase);
                        totalContainers.push(nameContainer);
                        cumplen = parseInt(item.CantidadCriteriosCumplen);
                        nocumplen = parseInt(item.CantidadCriteriosNoCumplen);
                        htmlData = "<div class='panel panel-default " + clase + "'><div class='panel-body'><ul><li>SI: " + cumplen + "</li><li>NO: " + nocumplen + "</li><li>Porcentaje de Adherencia: " + item.porcentajeAdherencia + "%</li><li>Nivel: " + item.umbralAdherencia + "</li></ul><div id='div_" + countContainer + "'><div id='" + nameContainer + "'></div></div></div></div>";
                        $('.modal-body').append(htmlData);
                    });
                    if (countContainer > 1) {
                        var ancho = 371 * countContainer;
                        $(".modal-dialog.modal-sm").css("width", ancho + "px");
                        $(".modal-dialog.modal-sm").css("overflow", "hidden");
                        $(".modal-body").css("height", "437");
                    } else {
                        $(".modal-dialog.modal-sm").css("width", "371px");
                    }

                    var len = 0;
                    for (var o in respuestaCalculoAdherencia) {
                        len++;
                    }

                    if (len > 1) {
                        $.each(totalclases, function(indexc, itemc) {
                            $("." + itemc).css("float", "left");
                            $("." + itemc).css("margin-right", "20px");
                        });
                    }

                    infoModal.modal('show');

                    var options = {
                        chart: {
                            plotBackgroundColor: null,
                            plotBorderWidth: null,
                            plotShadow: false,
                            type: 'pie',
                            width: 300,
                            height: 300
                        },
                        title: {
                            text: 'Representación Grafica'
                        },
                        tooltip: {
                            pointFormat: '{series.name}: <b>{point.percentage:.0f}%</b>'
                        },
                        plotOptions: {
                            pie: {
                                allowPointSelect: true,
                                cursor: 'pointer',
                                dataLabels: {
                                    enabled: true,
                                    format: '<b>{point.name}</b>: {point.percentage:.0f}%',
                                    style: {
                                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                    }
                                }
                            }
                        },
                        series: []
                    };

                    var contador = 0;
                    $.each(respuestaCalculoAdherencia, function(index, item) {

                        var data = new Object();

                        data = {
                            name: 'Porcentaje',
                            colorByPoint: true,
                            data: [{
                                name: 'Si',
                                y: parseInt(item.CantidadCriteriosCumplen)
                            }, {
                                name: 'No',
                                y: parseInt(item.CantidadCriteriosNoCumplen)
                            }]
                        };

                        options.chart.renderTo = totalContainers[contador];
                        options.series.push(data);
                        var chart = new Highcharts.Chart(options);
                        contador = contador + 1;
                        options.series = [];
                    });
                }
            });
        }

        var datosGenerales = {};
        datosGenerales["concecutivo"] = $('#concecutivo').val();
        datosGenerales["cedula"] = $('#cedulaPaciente').val();
        datosGenerales["dia"] = $('#fechaDiagnostico').val();
        datosGenerales["tdpaciente"] = $("#select_tp_dcmt option:selected").text();
        datosGenerales["ips"] = $("#ips option:selected").text();
        datosGenerales["servicio"] = $("#servicio option:selected").text();
        datosGenerales["barrio"] = $("#barrio option:selected").text();
        datosGenerales["estrato"] = $("#estrato option:selected").text();
        datosGenerales["diaEntraPaciente"] = $('#diaEntradaPaciente').val();
        datosGenerales["diaSalePaciente"] = $('#diaSalidaPaciente').val();
        datosGenerales["diasAtencionPaciente"] = $('#diasAtencion').val();
        datosGenerales["edadPaciente"] = $('#edadPaciente').val();
        datosGenerales["sexoPaciente"] = $("#sexoPaciente option:selected").text();
        datosGenerales["tipoRegimen"] = $("#tipoRegimen option:selected").text();

        var tipoListaSeleccionada = $("#tipoLista option:selected").text();

        if (tipoListaSeleccionada != "Pacientes") {
            var horaIngresoPaciente = $('#horaIngresoPaciente').val();
            var horaEgresoPaciente = $('#horaEgresoPaciente').val();

            datosGenerales["horaIngresoPaciente"] = horaIngresoPaciente;
            datosGenerales["horaEgresoPaciente"] = horaEgresoPaciente;
        }

        var lista_respuestas = [];
        $("select#respuesta").each(function() {
            var respuesta_x_item = new Object();
            var idcriterio = $(this).attr("idcriterio");
            var textoObservacion = $('input[idcriterio="' + idcriterio + '"]').val();
            var aplicaAdherencia = $('input#aplicaFormula[idcriterio="' + idcriterio + '"]').is(':checked');
            if (aplicaAdherencia) {
                aplicaAdherencia = 1;
            } else {
                aplicaAdherencia = 0;
            }
            respuesta_x_item.idCriterio = idcriterio;
            respuesta_x_item.respuestaCriterio = $(this).val();
            respuesta_x_item.observacionCriterio = textoObservacion;
            respuesta_x_item.aplicaCriterioAdherencia = aplicaAdherencia;
            lista_respuestas.push(respuesta_x_item);
        });

        var subLista = $("#tipoLista option:selected").attr("subLista");
        if (subLista == 1) {
            var subLista_respuestas = [];
            $("select#subRespuesta").each(function() {
                var respuesta_x_subitem = new Object();
                var idSubcriterio = $(this).attr("idsubcriterio");
                var textoObservacion = $('input[idsubcriterio="' + idSubcriterio + '"]').val();
                var aplicaAdherencia = $('input#subAplicaFormula[idsubcriterio="' + idSubcriterio + '"]').is(':checked');
                if (aplicaAdherencia) {
                    aplicaAdherencia = 1;
                } else {
                    aplicaAdherencia = 0;
                }
                respuesta_x_subitem.idSubCriterio = idSubcriterio;
                respuesta_x_subitem.respuestaCriterio = $(this).val();
                respuesta_x_subitem.observacionSubCriterio = textoObservacion;
                respuesta_x_subitem.aplicaSubCriterioAdherencia = aplicaAdherencia;
                subLista_respuestas.push(respuesta_x_subitem);
            });
        }

        var flagGuardar;

        for (campo in datosGenerales) {
            if (datosGenerales[campo] == "") {
                toastr.error("Alguno de los datos generales estan vacios, por favor revisarlos");
                flagGuardar = false;
                return;
            }
        }

        if (validarRespuestasLista(lista_respuestas)) {
            var tipoListaSeleccionada = $("#tipoLista option:selected").val();
            datosGenerales["idLista"] = tipoListaSeleccionada;
            flagGuardar = true;
        }
        if (subLista == 1) {
            flagGuardar = null;
            if (validarSubRespuestasLista(subLista_respuestas)) {
                flagGuardar = true;
            } else {
                flagGuardar = false;
            }

        }

        if (flagGuardar) {
            jsonDatosPersonales = JSON.stringify(datosGenerales);
            jsonListaRespuestas = JSON.stringify(lista_respuestas);
            var idPacienteGenerado;
            $.ajax({
                type: "POST",
                url: "BD/guardaDatosGenerales.php",
                data: {
                    'data': jsonDatosPersonales
                },
                dataType: "json",
                success: function(respuestaInsertPaciente) {
                    var tipoListaSeleccionada = $("#tipoLista option:selected").val();
                    toastr.success("Paciente guardado correctamente");
                    idPacienteGenerado = respuestaInsertPaciente.idPaciente;

                    $.ajax({
                        type: "POST",
                        url: "BD/insertRespuestas.php",
                        data: {
                            'data': jsonListaRespuestas,
                            'idPaciente': idPacienteGenerado,
                            'idLista': tipoListaSeleccionada
                        },
                        dataType: "json",
                        success: function(respuestaInsertRespuestas) {
                            var idSubLista;
                            if (respuestaInsertRespuestas.status) {
                                if (subLista == 1) {
                                    guardarRespuestasSubLista(subLista_respuestas, idPacienteGenerado);
                                    idSubLista = $("#tipoLista option:selected").val();
                                }
                                toastr.success("Respuestas guardadas correctamente");
                                calcularAdherencia(tipoListaSeleccionada, idPacienteGenerado, subLista, idSubLista);
                            }
                        }
                    });
                }
            });
        }
    });
});
