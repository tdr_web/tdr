<?php
require('conexion.php');

$listasubrespuestas = $_POST['data'];
$idPaciente = $_POST['idPaciente'];
$subrespuestasdecode = json_decode($listasubrespuestas, true);

foreach ($subrespuestasdecode as $item => $subcriterio) {
    $sqlSubRespuestas = 'CALL guardarRespuestas_X_SubLista('.mysqli_real_escape_string($enlace, $idPaciente).',
'.mysqli_real_escape_string($enlace, $subcriterio['idSubCriterio']).',
'.mysqli_real_escape_string($enlace, $subcriterio['respuestaCriterio']).',
"'.mysqli_real_escape_string($enlace, $subcriterio['observacionSubCriterio']).'",
"'.mysqli_real_escape_string($enlace, $subcriterio['aplicaSubCriterioAdherencia']).'")';

    $result_Subrespuestas = mysqli_query($enlace, $sqlSubRespuestas);
    if ($result_Subrespuestas) {
        mysqli_next_result($enlace);
    }
}
$respuesta['status'] = true;
echo json_encode($respuesta);
mysqli_close($enlace);
