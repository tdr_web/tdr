<?php
require('conexion.php');

$listarespuestas = $_POST['data'];
$idPaciente = $_POST['idPaciente'];
$idLista = $_POST['idLista'];
$respuestasdecode = json_decode($listarespuestas, true);

foreach ($respuestasdecode as $item => $criterio) {
    $sqlRespuestas = 'CALL guardarRespuestas('.mysqli_real_escape_string($enlace, $idPaciente).',
'.mysqli_real_escape_string($enlace, $criterio['idCriterio']).',
'.mysqli_real_escape_string($enlace, $criterio['respuestaCriterio']).',
"'.mysqli_real_escape_string($enlace, $criterio['observacionCriterio']).'",
'.mysqli_real_escape_string($enlace, $idLista).',
"'.mysqli_real_escape_string($enlace, $criterio['aplicaCriterioAdherencia']).'")';

    $result_respuestas = mysqli_query($enlace, $sqlRespuestas);
    if ($result_respuestas) {
        mysqli_next_result($enlace);
    }
}
$respuesta['status'] = true;
echo json_encode($respuesta);
mysqli_close($enlace);
